package chickenrun;

import java.util.Scanner;

public class Gestion {
	
	public Gestion() {
	
}
	public void AjoutVolaille() {
		
		int poulet = 0;
		int cannard = 0;
		int paon = 0;
		int type;
		int nb;
		
		Scanner in = new Scanner(System.in);
		
		System.out.println(" Veuillez entrer le type de volailles que vous souhaitez ajouter ");
		System.out.println(" 1 : Poulet ");
		System.out.println(" 2 : Cannard ");
		System.out.println(" 3 : Paon ");
		type = in.nextInt();
		System.out.println(" Veuillez entrer le nombre de volailles a ajouter");
		nb = in.nextInt();
		
		
		if(type == 1 && nb <= 5) {
			poulet = poulet + nb;
			System.out.println(" Ajout de " + nb + " Poulets effectu� ");
		}
		else if(type == 2 && nb <= 4) {
			cannard = cannard + nb;
			System.out.println(" Ajout de " + nb + " Cannards effectu� ");
		}
		else if(type == 3 && nb <= 3) {
			paon = paon + nb;
			System.out.println(" Ajout de " + nb + " Paons effectu� ");
		}
		else System.out.println(" Saisie incorrect, Recommencer ");
		
	}
	

	public void ModifPoidAbattage() {
		
		String volaille;
		double poidVol;
		
		Scanner in = new Scanner(System.in);
		
		System.out.println(" Veuillez entrer le Type de Volailles ");
		volaille = in.next();
		Volailles typeVol = new Volailles(volaille, 0, 0, 0);
		typeVol.setType(volaille);
		System.out.println(" Veuillez entrer le nouveau poids d'abattage ");
		poidVol = in.nextDouble();
		typeVol.setPoid(poidVol);
		System.out.println(" Changement effectu� ! " + typeVol.getType() + " " + typeVol.getPoid() + " Kg ");
		
		
	}
	
	public void  ModifPrixJour() {
		
		String volaille;
		float prix;
		
		Scanner in = new Scanner(System.in);
		
		System.out.println(" Veuillez entrer le Type de Volailles ");
		volaille = in.next();
		Volailles prixVol = new Volailles(volaille, 0, 0, 0);
		prixVol.setType(volaille);
		System.out.println(" Veuillez entrer le prix du jour ");
		prix = in.nextFloat();
		prixVol.setPrix(prix);
		System.out.println(" Changement effectu� ! " + prixVol.getType() + " " + prixVol.getPrix() + " Euros ");
		
		
	}
	
	public void ModifPoidVolaille() {
		
	}
	
	public void NombreVolailleParType() {
		
	}
	
	public void TotalPrixVolailleAbattable() {
		
	}
	
	public void VendreVolaille() {
		
	}
	
	public void RendrePaonAuParc() {
		
	}
}
