package chickenrun;

import java.util.Scanner;


public class Menu {

	public static void main(String[] args) {
		
		int choix = 0;
	
		Scanner in = new Scanner(System.in);
		
		System.out.println(" 1 Ajouter une Volaille ");
		System.out.println(" 2 Modifier poids Abattage ");
		System.out.println(" 3 Modifier prix du Jour ");
		System.out.println(" 4 Modifier poids d'une Volaille ");
		System.out.println(" 5 Voir le nombre de Volailles par Type ");
		System.out.println(" 6 Voir le Total de prix de Volailles Abattables ");
		System.out.println(" 7 Vendre une Volaille ");
		System.out.println(" 8 Rendre un Paon au Parc ");
		choix = in.nextInt();
		
		
		switch(choix){
		case 1:
			Gestion ajout = new Gestion();
			ajout.AjoutVolaille();
			break;
			
		case 2:
			Gestion poidaba = new Gestion();
			poidaba.ModifPoidAbattage();
			break;
			
		case 3:
			Gestion prixJour = new Gestion();
			prixJour.ModifPrixJour();
			break;
			
		case 4:
			Gestion modifpoidvol = new Gestion();
			modifpoidvol.ModifPoidVolaille();
			break;
			
		case 5:
			Gestion voirnombrevolailletype = new Gestion();
			voirnombrevolailletype.NombreVolailleParType();
			break;
			
		case 6:
			Gestion totalprixvolabattable = new Gestion();
			totalprixvolabattable.TotalPrixVolailleAbattable();
			break;
			
		case 7:
			Gestion vendrevol = new Gestion();
			vendrevol.VendreVolaille();
			break;
			
		case 8:
			Gestion rendrepaon = new Gestion();
			rendrepaon.RendrePaonAuParc();
			break;
			
			default: 
				System.out.println(" Saisie incorrect ");
			
		}
	}

}
