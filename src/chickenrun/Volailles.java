package chickenrun;

public class Volailles {
	
	private String type;
	private double poid;
	private double prix;
	private int id;
	
	public Volailles(String type, double poid, int id, double prix) {
		
		this.type = type;
		this.poid = poid;
		this.id = id;
		this.prix = prix;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getPoid() {
		return poid;
	}

	public void setPoid(double poid) {
		this.poid = poid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public double getPrix() {
		return prix;
	}
	
	public void setPrix(double prix) {
		this.prix = prix;
	}

}
